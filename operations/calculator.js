var number1 = null;
var operation = null;

function numberButton(n){
    result = document.getElementById("result");
    result.innerHTML = result.textContent + n;
}

function delButton(){
    result = document.getElementById("result");
    result.innerHTML = "";
    number1 = null;
    operation = null;
}

function changeSignButton(){
    result = document.getElementById("result");
    result.innerHTML = operate(multiply, result.textContent, -1);
}

function submitButton(){

    result = document.getElementById("result");

    number2 = result.textContent;

    result.innerHTML = operate(operation, number1, number2);
    
}

function sumButton(){
    
    result = document.getElementById("result");
    
    number1 = result.textContent;
    operation = sum;
    
    result.innerHTML = "";
    
}

function subtractButton(){
    
    result = document.getElementById("result");
    
    number1 = result.textContent;
    operation = subtract;
    
    result.innerHTML = "";
    
}

function multiplyButton(){
    
    result = document.getElementById("result");
    
    number1 = result.textContent;
    operation = multiply;
    
    result.innerHTML = "";
    
}

function divideButton(){
    
    result = document.getElementById("result");
    
    number1 = result.textContent;
    operation = divide;
    
    result.innerHTML = "";
    
}


function squareRootButton(){
    
    result = document.getElementById("result");

    number = result.textContent;

    result.innerHTML = operate(squareRoot, number);
    
}