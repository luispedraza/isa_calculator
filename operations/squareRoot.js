﻿// Basado en la función de aproximación de Bakhshali: https://es.wikipedia.org/wiki/C%C3%A1lculo_de_la_ra%C3%ADz_cuadrada#Aproximaci%C3%B3n_de_Bakhshali 

const squareRoot = (x) =>{
   //contemplar la excepción de la aproximación de Bakhshali
   if(x==0){
       return 0;
   }
   
    // Valores por defecto
    keepSearch = true;
    n = 0;
    i=0;
    lastN=0;
    xi=0;
    
    do{
        // Multiplicamos
        xi=i*i;
    
        // Si es mayor a x es que nos hemos acercado mucho
        if(xi>=x){
    
            // Calculamos distancias
            num1 = xi-x;
            num2 = lastN-x;
    
            // Pasamos ambos a valores absolutos
            if(num1<0){
                num1 = num1*-1;
            }
    
            if(num2<0){
                num2 = num2*-1;
            }
    
            // Vemos con cual nos quedamos, si hay más distancia en el número 1, nos quedamos con lastN
            if(num1 > num2){
                n=i-1;
            }else{
                n=i;
            }
    
            // Paramos de buscar
            keepSearch = false;
    
        }
    
        // Guardamos n como el anterior
        lastN = xi;
    
        // Aumentamos i en 1
        i++;
    
    }while(keepSearch);

    var bakhs = (n**4 + (6*(n**2)*x) + x**2) / ((4*n**3) + (4*n*x));
    return parseFloat(bakhs.toFixed(5));

}
module.exports=squareRoot;