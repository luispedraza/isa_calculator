const {test, expect} = require("@jest/globals");
const subtract = require("../operations/subtract");

test("se resta 10 a 4. Debería devolver 6", () =>{
    //arrange
    const x = 10;
    const y = 4;
    const expected = 6; 

    //act
    const result = subtract(x,y);

    //assert
    expect(result).toBe(expected);
});