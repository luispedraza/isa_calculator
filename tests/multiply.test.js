const {test, expect} = require("@jest/globals");
const multiply = require("../operations/multiply");

test("se multiplica 4 y 5. Debería devolver 20", () =>{
    //arrange
    const x = 4;
    const y = 5;
    const expected = 20; 

    //act
    const result = multiply(x,y);

    //assert
    expect(result).toBe(expected);
});