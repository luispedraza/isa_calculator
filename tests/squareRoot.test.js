const {test, expect} = require("@jest/globals");
const squareRoot = require("../operations/squareRoot");

test("se hace la raiz cuadrada de 82. Debería devolver 9.05539", () =>{
    //arrange
    const x = 82;
    const expected = 9.05539; 

    //act
    const result = squareRoot(x);

    //assert
    expect(result).toBe(expected);
});