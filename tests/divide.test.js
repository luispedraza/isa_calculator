const {test, expect} = require("@jest/globals");
const divide = require("../operations/divide");

test("se dividie 100 entre 2. Debería devolver 50", () =>{
    //arrange
    const x = 100;
    const y = 2;
    const expected = 50; 

    //act
    const result = divide(x,y);

    //assert
    expect(result).toBe(expected);
});