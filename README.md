# Laboratorio 2: Calculadora

## Introducción

Para el desarrollo de la calculadora hemos utilizado TDD, que consiste en desarrollar el test y después implementar la funcionalidad en sí.

Queríamos usar una tecnología que no hubiéramos utilizando previamente para aprovechar y poder aprender algo nuevo, a ambos nos llamaba la atención las tecnologías web, pero preferíamos hacer una aplicación de escritorio y no una web.  Buscamos un framework que nos permitiera tecnologías web en escritorio y encontramos [Electron](https://www.electronjs.org/).

[Electron](https://www.electronjs.org/) no es un proyecto reciente, de hecho nació en 2013 y muchas aplicaciones famosas como Visual Studio Code o WhatsApp Desktop lo utilizan. Además de la ventaja de poder utilizar tecnologías web como HTML5, CSS y JS para el desarrollo, evitamos tener que realizar diferentes desarrollos para cada plataforma, ya podemos exportar la aplicación para Windows, Mac y Linux.

Lo primero que hicimos fue crear el repositorio en [Bitbucket](https://bitbucket.org/uxueGB/isa_calculator/src/master/README.md), configurar git en nuestros equipos y clonar el repositorio creado. También fue necesario instalar [Node.js](https://nodejs.org/es/) y [npm](https://www.npmjs.com/) ya que era una dependencia para utilizar Electron.

Una vez tuvimos nuestro entorno de desarrollo preparado, acudimos a la documentación de Electron y seguimos los pasos del [Getting Started](https://www.electronjs.org/docs/tutorial/introduction) para tener un proyecto base en el que poder empezar a trabajar.

Cuando tuvimos un "Hola Mundo", nos pusimos manos a la obra a crear nuestro primer test. Como estamos utilizando la metodología de desarrollo TDD, cada vez que realizábamos un test, hacíamos la implementación de aquello que habíamos "probado" previamente. Para realizar los tests hemos utilizando la librería [Jest](https://jestjs.io/es-ES/) que vimos durante el laboratorio.

Las implementaciones de la suma, resta, multiplicación y división, fueron fáciles. Sin embargo, para la implementación de la raíz cuadrada, tuvimos que recurrir al foro de la asignatura para plantear nuestras dudas. Un compañero nos enseñó la [función de aproximación de Bakhshali](https://es.wikipedia.org/wiki/C%C3%A1lculo_de_la_ra%C3%ADz_cuadrada#Aproximaci%C3%B3n_de_Bakhshali) y la implementamos. También intentamos incluir las funciones de suma, multiplicación y división dentro de la propia función de raíz cuadrada, pero una vez habíamos pasado todo el código, el test no funcionaba (no reconocía las funciones), y si incluíamos las dependencias, la interfaz dejaba de funcionar, tras varios intentos fallidos, decidimos hacer un ``git reset`` y dejar la primera versión de la función de raíz cuadrada. Este fue nuestro intento de código que no funcionó:

```javascript
var bakhs = divide((sum(sum( multiply(n, multiply(n, multiply(n, n))), (multiply(multiply(6,(multiply(n, n))),x))), multiply(x, x))), (sum((multiply(4, multiply(n, multiply(n, n)))), (multiply(4,multiply(n,x))))));
```

La parte más entretenida del trabajo fue realizar la interfaz gráfica, para realizarla nos apoyamos en [Bootstrap](https://getbootstrap.com/) que es un framework que nos permitió "no reinventar la rueda" y hacer el trabajo de maquetado mucho más sencillo.

Una vez estuvimos contentos con el diseño, incluimos interactividad a los botones, uniendo el código que habíamos implementado para cada una de las operaciones matemáticas con la propia interfaz.

El resultado ha sido una calculadora sencilla, pero funcional.

<img src="https://i.ibb.co/QJq7dCb/Screenshot-2.png" style="zoom: 33%;" />

## Instrucciones de ejecución

Para poder descargar y ejecutar el proyecto de la calculadora, se deben seguir los siguientes pasos a través de la terminal de comandos.

### Paso 1: Clonar el repositorio
```bash
git clone https://bitbucket.org/uxueGB/isa_calculator.git
```

### Paso 2:  Entrar en el directorio
```bash
cd isa_calculator
```

### Paso 3:  Descargar dependencias
```bash
npm install
```

### Paso 4:  Ejecutar el proyecto
```bash
npm start
```

### Paso 5 :  ¡A calcular! 🧮

## Autores

**Uxue Goldaraz Bermejo**

**Antonio Torres García**


## Referencias

- Colaboradores de Wikipedia. (2020, 19 noviembre). Cálculo de la raíz cuadrada. Wikipedia, la enciclopedia libre. https://es.wikipedia.org/wiki/C%C3%A1lculo_de_la_ra%C3%ADz_cuadrada#Aproximaci%C3%B3n_de_Bakhshali

- Colaboradores de Wikipedia. (2021, 22 mayo). Electron (software). Wikipedia, la enciclopedia libre. https://es.wikipedia.org/wiki/Electron_(software)
  Empezando · Jest. (s. f.). Jest. Recuperado 26 de mayo de 2021, de https://jestjs.io/es-ES/docs/getting-started

- Iker Izaguirre Ayerregaray. (2021, 25 mayo). Foro Asignatura. Pregunta Laboratorio 2. https://campusvirtual.unir.net/portal/site/PER1518-2-2177-222/tool/2bcd9304-aa16-47f2-88c4-282fdea74811/discussionForum/message/dfViewMessage

- L. (s. f.). How to set the windows size fixed. Atom Discussion. Recuperado 26 de mayo de 2021, de https://discuss.atom.io/t/how-to-set-the-windows-size-fixed/22088

- Otto, M. J. T. (s. f.). Grid system. Bootstrap. Recuperado 26 de mayo de 2021, de https://getbootstrap.com/docs/5.0/layout/grid/#row-columns

- Quick Start | Electron. (s. f.). Electron. Recuperado 26 de mayo de 2021, de https://www.electronjs.org/docs/tutorial/quick-start